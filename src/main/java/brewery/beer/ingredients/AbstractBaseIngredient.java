package brewery.beer.ingredients;

public abstract class AbstractBaseIngredient {
    double amountInGram;

    AbstractBaseIngredient() {
    }

    AbstractBaseIngredient(double amountInGram) {
        this.amountInGram = amountInGram;
    }

    public double getAmountInGram() {
        return amountInGram;
    }

    public void setAmountInGram(double amountInGram) {
        this.amountInGram = amountInGram;
    }
}
