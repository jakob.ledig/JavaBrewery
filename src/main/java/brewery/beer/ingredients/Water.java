package brewery.beer.ingredients;

/**
 * Water of which ever origin you like. Hardness takes impact on the taste of the beer.
 */
public class Water extends AbstractBaseIngredient {
    private int temperature;

    public Water(int temperature, double amountInLiters) {
        super(amountInLiters);
        this.temperature = temperature;
    }

    public int getTemperature() {
        return temperature;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public double getAmountInLiters() {
        return this.amountInGram / 1000;
    }

    @Override
    public String toString() {
        return "Water{" +
            amountInGram + " Liters, " +
            "temperature=" + temperature + "°C" +
            '}';
    }
}