package brewery.beer.ingredients;

import brewery.logic.YeastFermentation;

/**
 * Micro organisms turning sugar into alcohol and CO2.
 * Can be either top- or bottom-fermenting, which strongly determines the beer style.
 * Base for all alcoholic beverages known to mankind.
 */
public class Yeast extends AbstractBaseIngredient {
    private YeastFermentation fermentation;
    private double temperatureOptimum;

    public Yeast(double amount, YeastFermentation fermentation) {
        super(amount);
        this.fermentation = fermentation;

        if (fermentation == YeastFermentation.BOTTOM_FERMENTING) {
            temperatureOptimum = 5;
        } else {
            temperatureOptimum = 20;
        }
    }

    public YeastFermentation getFermentation() {
        return fermentation;
    }

    public double getTemperatureOptimum() {
        return temperatureOptimum;
    }

    public void setTemperatureOptimum(double temperatureOptimum) {
        this.temperatureOptimum = temperatureOptimum;
    }

    public void setFermentation(YeastFermentation fermentation) {
        this.fermentation = fermentation;
    }
}
