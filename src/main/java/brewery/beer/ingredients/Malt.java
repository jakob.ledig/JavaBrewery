package brewery.beer.ingredients;

/**
 * A malt, being what ever kind of grain you want, bruised.
 * Determines what kind of beer we produce.
 * Special malts exist like smoked malt.
 * Augustiner brewery is famous for it's malts.
 * Gave name to a profession and a famous TV cook.
 */
public class Malt extends AbstractBaseIngredient {

    public Malt(double amountInGram) {
        super(amountInGram);
    }

    public Malt() {
    }

    @Override
    public String toString() {
        return "Malt{" + "amountInGram=" + amountInGram + " grams, " +
                '}';
    }
}
