package brewery.beer.ingredients;
public class Addition extends AbstractBaseIngredient {
    public Addition(double amount) {
        super(amount);
    }
}
