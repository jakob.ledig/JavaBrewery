package brewery.beer.ingredients;

/**
 * Refined sugar or honey. If the latter, add 1/5th.
 */
public class Sugar extends AbstractBaseIngredient {
    public Sugar(double amountInGram) {
        super(amountInGram);
    }
}
