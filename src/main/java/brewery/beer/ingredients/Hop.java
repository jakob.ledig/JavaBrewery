package brewery.beer.ingredients;

/**
 * Directly related to hemp, this ingredient contributes both strongly to the taste as well as to
 * the beer's storage life.
 */

public class Hop extends AbstractBaseIngredient {

    public Hop(double amount) {
        super(amount);
    }

    public Hop() {
    }

    @Override
    public String toString() {
        return "Hop{" +
            "amountInGram=" + amountInGram + " Grams, " +
            '}';
    }
}