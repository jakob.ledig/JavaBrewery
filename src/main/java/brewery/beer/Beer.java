package brewery.beer;

import java.util.List;

import brewery.beer.ingredients.Addition;
import brewery.beer.ingredients.Hop;
import brewery.beer.ingredients.Malt;
import brewery.beer.ingredients.Water;
import brewery.beer.ingredients.Yeast;

/**
 * Pure liquid gold (or amber, ebony, ...).
 * A beverage as old as civilisation.
 * First human buildings already show hints on beer.
 * Apparently, a direct product of the development of agriculture.
 * Discovered by every ancient civilization which had also bread.
 * Deciding to make beer or bread is done by choosing how close
 * one puts his/her bowl of porridge to the fire.
 */
public class Beer {
    private Water water;
    private List<Malt> malts;
    private List<Hop> hops;
    private Yeast yeast;
    private List<Addition> additions;

    private double originalWort; // "Stammwuerze"
    private double volAlcohol;
    private double co2PerLitre;

    public Beer(Water water, List<Malt> malts, List<Hop> hops, Yeast yeast, List<Addition> additions) {
        this.water = water;
        this.malts = malts;
        this.hops = hops;
        this.yeast = yeast;
        this.additions = additions;
    }

    public Water getWater() {
        return water;
    }

    public List<Malt> getMalts() {
        return malts;
    }

    public List<Hop> getHops() {
        return hops;
    }

    public Yeast getYeast() {
        return yeast;
    }

    public List<Addition> getAdditions() {
        return additions;
    }

    public double getOriginalWort() {
        return originalWort;
    }

    public void setOriginalWort(double originalWort) {
        this.originalWort = originalWort;
    }

    public double getVolAlcohol() {
        return volAlcohol;
    }

    public void setVolAlcohol(double volAlcohol) {
        this.volAlcohol = volAlcohol;
    }

    public double getCo2PerLitre() {
        return co2PerLitre;
    }

    public void setCo2PerLitre(double co2PerLitre) {
        this.co2PerLitre = co2PerLitre;
    }

    @Override
    public String toString() {
        return "Beer{" +
            water.getAmountInLiters() + " Liters, " +
            "originalWort: " + originalWort + ", " +
            "CO2 per litre: " + co2PerLitre + ", " +
            '}';
    }
}
