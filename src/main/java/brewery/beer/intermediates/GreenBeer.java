package brewery.beer.intermediates;

import java.util.List;

import brewery.beer.Beer;
import brewery.beer.ingredients.Addition;
import brewery.beer.ingredients.Hop;
import brewery.beer.ingredients.Malt;
import brewery.beer.ingredients.Water;
import brewery.beer.ingredients.Yeast;

/**
 * This is, in theory, already a beer, yet it does not contain CO2 yet.
 */
public class GreenBeer extends Beer {

    public GreenBeer(Water water, List<Malt> malts, List<Hop> hops, Yeast yeast, List<Addition> additions) {
        super(water, malts, hops, yeast, additions);
    }

    public GreenBeer(Wort wort) {
        super(wort.getWater(), wort.getMalts(), wort.getHops(), wort.getYeast(), wort.getAdditions());
    }
}
