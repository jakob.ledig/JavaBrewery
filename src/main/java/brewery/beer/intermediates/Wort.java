package brewery.beer.intermediates;

import java.util.ArrayList;
import java.util.List;

import brewery.beer.ingredients.Addition;
import brewery.beer.ingredients.Hop;
import brewery.beer.ingredients.Malt;
import brewery.beer.ingredients.Water;
import brewery.beer.ingredients.Yeast;

/**
 * Liquid containing the sugar from the grains. German: Würze
 */
public class Wort {
    private Water water;
    private final List<Malt> malts;
    private final double metricVolume;
    private List<Hop> hops;
    private List<Addition> additions;
    private Yeast yeast;
    private double temperature;
    private boolean lautered;

    public Wort(Water water, List<Malt> malts, double amountInGram, double temperature) {
        this.water = water;
        this.malts = malts;
        this.metricVolume = amountInGram / 1000;
        this.temperature = temperature;
        this.hops = new ArrayList<>();
    }

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    public boolean isLautered() {
        return lautered;
    }

    public void setLautered(boolean lautered) {
        this.lautered = lautered;
    }

    public List<Malt> getMalts() {
        return malts;
    }

    public double getMetricVolume() {
        return metricVolume;
    }

    public List<Hop> getHops() {
        return hops;
    }

    public void setHops(List<Hop> hops) {
        this.hops = hops;
    }

    public void addHop(Hop hop) {
        this.hops.add(hop);
    }

    public Water getWater() {
        return water;
    }

    public void setWater(Water water) {
        this.water = water;
    }

    public List<Addition> getAdditions() {
        return additions;
    }

    public void setAdditions(List<Addition> additions) {
        this.additions = additions;
    }

    public Yeast getYeast() {
        return yeast;
    }

    public void setYeast(Yeast yeast) {
        this.yeast = yeast;
    }

    @Override
    public String toString() {
        return "Wort{" + "volume:" + metricVolume + " liters, " + "malts: " + malts.toString() + ", " + "hops: " + (
            hops == null ? "none" : hops.toString()) + ", " + "temperature: " + temperature + '}';
    }
}
