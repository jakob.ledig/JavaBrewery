package brewery.beer.intermediates;

import brewery.beer.ingredients.Malt;
import brewery.beer.ingredients.Water;

import java.util.List;

/**
 * This is basically water with grains. Already tastes sweet. German: Maische
 */
public class Mash {
    private final Water water;
    private final List<Malt> malts;
    private double temperature;

    public Mash(Water water, List<Malt> malts, double temperature) {
        this.water = water;
        this.malts = malts;
        this.temperature = temperature;
    }

    public Water getWater() {
        return water;
    }

    public List<Malt> getMalts() {
        return malts;
    }

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }
}
