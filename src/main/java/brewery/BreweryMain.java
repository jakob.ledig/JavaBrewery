package brewery;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import brewery.beer.Beer;
import brewery.beer.ingredients.Hop;
import brewery.beer.ingredients.Malt;
import brewery.beer.ingredients.Sugar;
import brewery.beer.ingredients.Water;
import brewery.beer.ingredients.Yeast;
import brewery.beer.intermediates.GreenBeer;
import brewery.beer.intermediates.Mash;
import brewery.beer.intermediates.Wort;
import brewery.breweries.Kitchen;
import brewery.logic.Rest;
import brewery.logic.YeastFermentation;
import brewery.logic.exceptions.BloodyMessException;
import brewery.logic.exceptions.NotInStockException;
import brewery.logic.exceptions.WortTooHotException;
import brewery.misc.Shop;
import brewery.tools.Copper;
import brewery.tools.Fermenter;
import brewery.tools.MashFilter;

public class BreweryMain {
    /**
     * Brewing a "Deutsches Helles" in our kitchen!
     */
    public static void main(String[] args) throws BloodyMessException, NotInStockException, WortTooHotException {
        Logger logger = LoggerFactory.getLogger("BreweryMainLogger");
        // ordering tools
        Shop hopfenUndMehr = new Shop("https://www.hobbybrauerversand.de/");
        Copper punchCooker = hopfenUndMehr.buyCopper(30);
        Fermenter bucket = hopfenUndMehr.buyFermenter(30);
        MashFilter mashFilter = hopfenUndMehr.buy(MashFilter.class);

        // ordering ingredients
        Malt pilsenerMalz = hopfenUndMehr.buy(Malt.class, 4500);
        Malt caraMalz = hopfenUndMehr.buy(Malt.class, 500);
        Hop perle = hopfenUndMehr.buy(Hop.class, 16.7);
        Yeast danstarNottingHamAle = hopfenUndMehr.buy(11, YeastFermentation.TOP_FERMENTING);

        // setting everything up
        Water water = new Water(70, 20); // 70° is the initial mashing temperature
        Kitchen kitchen = new Kitchen(punchCooker, bucket, mashFilter);
        Rest kombiRast = new Rest(66, 90);

        // the actual work
        Mash mash = kitchen.mash(water, Arrays.asList(pilsenerMalz, caraMalz), Collections.singletonList(kombiRast));
        Wort wort = kitchen.lauter(mash);
        Wort cookedWort = kitchen.boil(wort, Collections.singletonList(perle), new ArrayList<>(), 90);

        // keep everything neatly clean after this point!
        Wort cooledDownWort = kitchen.coolDown(cookedWort, 10);
        GreenBeer greenBeer = kitchen.ferment(cooledDownWort, danstarNottingHamAle);

        Sugar sugar = new Sugar(8 * water.getAmountInLiters());
        Beer deutschesHelles = kitchen.condition(greenBeer, sugar, 5.5, 15);
        logger.info("Our beer is done: {} liters of Deutsches Helles: {}", deutschesHelles.getWater().getAmountInLiters(), deutschesHelles.toString());
    }
}

