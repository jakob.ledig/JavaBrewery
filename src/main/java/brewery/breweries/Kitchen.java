package brewery.breweries;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import brewery.beer.Beer;
import brewery.beer.ingredients.Addition;
import brewery.beer.ingredients.Hop;
import brewery.beer.ingredients.Malt;
import brewery.beer.ingredients.Sugar;
import brewery.beer.ingredients.Water;
import brewery.beer.ingredients.Yeast;
import brewery.beer.intermediates.GreenBeer;
import brewery.beer.intermediates.Mash;
import brewery.beer.intermediates.Wort;
import brewery.logic.IBrewery;
import brewery.logic.Rest;
import brewery.logic.exceptions.BloodyMessException;
import brewery.logic.exceptions.WortTooHotException;
import brewery.tools.Copper;
import brewery.tools.Fermenter;
import brewery.tools.MashFilter;
import brewery.tools.Towel;

public class Kitchen implements IBrewery {
    private Copper punchCooker;
    private Fermenter bucket;
    private Towel towel;
    private MashFilter lauteringPlate;

    private Logger logger;

    public Kitchen(Copper punchCooker, Fermenter bucket, MashFilter lauteringPlate) {
        this.punchCooker = punchCooker;
        this.lauteringPlate = lauteringPlate;
        this.bucket = bucket;
        logger = LoggerFactory.getLogger("Kitchen Logger");
    }

    public Kitchen(Copper punchCooker, Fermenter bucket, Towel towel) {
        this.punchCooker = punchCooker;
        this.towel = towel;
        this.bucket = bucket;
        logger = LoggerFactory.getLogger("Kitchen Logger");
    }

    @Override
    public Mash mash(Water water, List<Malt> malts, List<Rest> rests) throws BloodyMessException {
        if (punchCooker == null || this.punchCooker.getCapacity() < water.getAmountInLiters()) {
            throw new BloodyMessException("Jesus! Everything is now on the kitchen floor!");
        }

        logger.info("Mashing at {} °C", water.getTemperature());
        Mash mash = new Mash(water, malts, water.getTemperature());
        for (Rest rest : rests) {
            logger.info("Resting at {}°C for {} minutes", rest.getTemperature(), rest.getDurationInMinutes());
            mash.setTemperature(rest.getTemperature());
        }
        mash.setTemperature(76);
        return mash;
    }

    @Override
    public Wort lauter(Mash mash) throws BloodyMessException {
        Wort wort;
        if (bucket.getCapacityInLiters() < mash.getWater().getAmountInLiters()) {
            throw new BloodyMessException("Damn it, the bucket is too small!");
        }
        if (this.lauteringPlate == null) { // meaning we only have a towel to lauter
            Random random = new Random();
            if (random.nextBoolean()) {
                throw new BloodyMessException("Fuck this shit, I'm buying a lautering plate");
            }
            wort = towel.filter(mash);
        } else {
            wort = lauteringPlate.filter(mash);
        }
        return wort;
    }

    @Override
    public Wort boil(Wort wort, List<Hop> hops, List<Addition> additions, long durationInMinutes) {
        List<Hop> listOfHops = new LinkedList<>(hops);
        Hop bitterHop = listOfHops.remove(0);
        wort.addHop(bitterHop);
        wort.setTemperature(100);
        logger.info("Boiling for {} minutes", durationInMinutes - 10);
        for (Hop hop : hops) {
            wort.addHop(hop);
        }
        logger.info("Added the rest of the hops, boiling now for 10 more minutes");
        return wort;
    }

    @Override
    public GreenBeer ferment(Wort wort, Yeast yeast) throws WortTooHotException {
        wort.setYeast(yeast);
        logger.info("Added {} to our wort", yeast.toString());
        if (wort.getTemperature() >= 50) {
            throw new WortTooHotException("You just killed your yeast. So sad!");
        }
        logger.info("Fermenting several days until no CO2 rises from the bucket");
        wort.setTemperature(yeast.getTemperatureOptimum());
        return new GreenBeer(wort);
    }

    @Override
    public Beer condition(Beer beer, Sugar sugar, double co2PerLitre, double originalWort) {
        logger.info("Adding {} grams of sugar to our {} liters of green beer", sugar.getAmountInGram(), beer.getWater().getAmountInLiters());
        beer.setOriginalWort(originalWort);
        beer.setVolAlcohol(5.0);
        beer.setCo2PerLitre(co2PerLitre);
        logger.info("After 10 days, our beer is ready to consume or further maturing.");
        return beer;
    }

    public Wort coolDown(Wort wort, long durationInHours) {
        logger.info("Cooling down for {} hours", durationInHours);
        wort.setTemperature(wort.getTemperature() / durationInHours);
        return wort;
    }
}
