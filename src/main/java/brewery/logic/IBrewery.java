package brewery.logic;

import java.util.List;

import brewery.beer.Beer;
import brewery.beer.ingredients.Addition;
import brewery.beer.ingredients.Hop;
import brewery.beer.ingredients.Malt;
import brewery.beer.ingredients.Sugar;
import brewery.beer.ingredients.Water;
import brewery.beer.ingredients.Yeast;
import brewery.beer.intermediates.Mash;
import brewery.beer.intermediates.Wort;
import brewery.logic.exceptions.BloodyMessException;
import brewery.logic.exceptions.WortTooHotException;

/**
 * Declares the methods every brewery has to implement in order to brew beer.
 */
public interface IBrewery {

    /**
     * Describes the process of breaking down starch into sugars by the enzymes in the malt. Can have multiple rests or only one.
     * German: Maischen
     *
     * @param water Brewing water or tap water in most home contexts
     * @param malts Blend of different malts
     * @return Hot mash
     */
    Mash mash(Water water, List<Malt> malts, List<Rest> rests) throws BloodyMessException;


    /**
     * Cleansing the mash from the grain residue.
     * Remember to let the wort rest for 20 minutes so the malts can settle.
     *
     * @param mash Mash with floating malt remnants
     * @return Cleansed Wort, ready to be boiled
     */
    Wort lauter(Mash mash) throws BloodyMessException;


    /**
     * Cooks the wort with the hops and any given additions.
     * If the German purity law is of importance, no additions may be used.
     *
     * @param wort              The base substance from the last step
     * @param hops              May be a combination of hops, just one or none at all (empty list then).
     * @param additions         Pass an empty list in here if not required.
     * @param durationInMinutes How long to boil?
     * @return A wort that is ready to be fermented
     */
    Wort boil(Wort wort, List<Hop> hops, List<Addition> additions, long durationInMinutes);

    /**
     * Lets the yeast do it's work.
     *
     * @param wort  The cooked wort now containing hops and optional additions.
     * @param yeast Be careful not to kill the yeast with a hot wort!
     * @return Something that may already be beer
     */
    Beer ferment(Wort wort, Yeast yeast) throws WortTooHotException;

    /**
     * An additional step, depending on the beer style and the available tools.
     * In home brewing contexts, this will mostly be bottle fermentation for carbonation.
     *
     * @param beer  A young beer from before
     * @param sugar Dissolved in a liquid to give the yeast additional food
     * @return Conditioned, sparkling, matured pure gold!
     */
    Beer condition(Beer beer, Sugar sugar, double co2PerLitre, double originalWort);
}
