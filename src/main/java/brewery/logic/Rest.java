package brewery.logic;

/**
 * A time frame in which a specific temperature is held in the mash.
 * The temperature determines which enzymes work best, resulting in
 * more or less complex sugars, which are not all as easy to consume
 * bye the yeast.
 */
public class Rest {
    private final long durationInMinutes;
    private double temperature;

    public Rest(long temperature, long durationInMinutes) {
        this.temperature = temperature;
        this.durationInMinutes = durationInMinutes;
    }

    public double getTemperature() {
        return temperature;
    }

    public long getDurationInMinutes() {
        return durationInMinutes;
    }
}
