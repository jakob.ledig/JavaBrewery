package brewery.logic.exceptions;
public class WortTooHotException extends Exception {
    public WortTooHotException(String message) {
        super(message);
    }
}
