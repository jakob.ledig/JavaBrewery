package brewery.logic.exceptions;

public class NotInStockException extends Exception {
    public NotInStockException() {
        super("Sorry, the ingredient is not in store!");
    }

    public NotInStockException(String message) {
        super("Sorry, the ingredient is not in store! \n" + message);
    }
}
