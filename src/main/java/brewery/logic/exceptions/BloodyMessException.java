package brewery.logic.exceptions;

public class BloodyMessException extends Exception {
    public BloodyMessException(String s) {
        super(s);
    }
}
