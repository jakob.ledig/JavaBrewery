package brewery.logic;
public enum YeastFermentation {
    TOP_FERMENTING,
    BOTTOM_FERMENTING
}
