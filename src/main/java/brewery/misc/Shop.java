package brewery.misc;

import brewery.beer.ingredients.AbstractBaseIngredient;
import brewery.beer.ingredients.Yeast;
import brewery.logic.YeastFermentation;
import brewery.logic.exceptions.NotInStockException;
import brewery.tools.AbstractBaseTool;
import brewery.tools.Copper;
import brewery.tools.Fermenter;

public class Shop {

    private String url;

    public Shop(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public Yeast buy(double amountInGram, YeastFermentation fermentation) {
        return new Yeast(amountInGram, fermentation);
    }

    public Copper buyCopper(double capacityInLiters) {
        return new Copper(capacityInLiters);
    }

    public Fermenter buyFermenter(double capacityInLiters) {
        return new Fermenter(capacityInLiters);
    }

    public <T extends AbstractBaseIngredient> T buy(Class<T> ingredientType, double amountInGram) throws
                                                                                                  NotInStockException {
        try {
            T ingredient = ingredientType.newInstance();
            ingredient.setAmountInGram(amountInGram);
            return ingredient;
        } catch (InstantiationException | IllegalAccessException e) {
            throw new NotInStockException(e.getMessage());
        }
    }

    public <T extends AbstractBaseTool> T buy(Class<T> toolType) throws NotInStockException {
        try {
            return toolType.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new NotInStockException(e.getMessage());
        }
    }
}
