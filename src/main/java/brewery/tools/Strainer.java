package brewery.tools;

import brewery.beer.intermediates.Mash;
import brewery.beer.intermediates.Wort;

/**
 * Device to separate the grain and the Wort/Mash.
 */
abstract class Strainer extends AbstractBaseTool {
    private boolean good;

    Strainer(boolean good) {
        this.good = good;
    }

    public Wort filter(Mash mash) {
        return new Wort(mash.getWater(), mash.getMalts(), mash.getWater().getAmountInGram(), mash.getTemperature());
    }
}
