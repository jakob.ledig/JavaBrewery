package brewery.tools;

/**
 * A roof shaped metal sheet with holes for the brew to be separated from the grain.
 */
public class MashFilter extends Strainer {
    public MashFilter() {
        super(true);
    }
}
