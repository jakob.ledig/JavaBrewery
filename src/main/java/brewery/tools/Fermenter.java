package brewery.tools;
public class Fermenter extends AbstractBaseTool {
    private double capacityInLiters;

    public Fermenter(double capacityInLiters) {
        this.capacityInLiters = capacityInLiters;
    }

    public double getCapacityInLiters() {
        return capacityInLiters;
    }

    public void setCapacityInLiters(double capacityInLiters) {
        this.capacityInLiters = capacityInLiters;
    }
}
