package brewery.tools;

/**
 * Containment for hot liquids. Brewing kettle used for mashing and hop cooking.
 */
public class Copper extends AbstractBaseTool {
    private double capacity;

    public Copper(double capacity) {
        this.capacity = capacity;
    }

    public double getCapacity() {
        return capacity;
    }
}
