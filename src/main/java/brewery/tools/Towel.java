package brewery.tools;

/**
 * A thin kitchen towel.
 */
public class Towel extends Strainer {
    public Towel() {
        super(false);
    }
}
