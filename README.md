# private Beer brew();

An object oriented approach on brewing beer at home

## We'll be looking at:

- ingredients
- history of beer
- processes
- tools
- trivia & anecdotes

## URLs
### Shop

- Hopfen und Mehr:
    - https://www.hobbybrauerversand.de/
    
### Websites

- http://fabier.de/biercalcs.html
- https://hobbybrauer.de/forum/wiki/doku.php/doku
- https://brauanleitung.wordpress.com/

### Citations

- Science Slam: "Sesshaft dank Bier":
    - https://www.youtube.com/watch?v=IFHDkqgaGOY
- Andreas Bok (Bok-Bier, CCC) in Podcasts:
    - https://cre.fm/cre194-bier
    - https://blog.richter.fm/podcast/diewahrheit/20120502/die-wahrheit-012-bier
    - https://wrint.de/2012/12/08/wr131-ortsgesprach-andreas-bogk/